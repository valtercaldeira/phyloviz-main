/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.phyloviz.viewer.ui;

import net.phyloviz.core.data.InfoEdge;
import net.phyloviz.core.data.InfoNode;
import prefuse.action.layout.graph.ForceDirectedLayout;
import prefuse.visual.EdgeItem;

/**
 *
 * @author Valter
 */
public class ForceLayout extends ForceDirectedLayout {

    public ForceLayout(String string) {
        super(string);
    }
    
    @Override
    public float getSpringLength(EdgeItem e){
        InfoEdge<InfoNode> edge = (InfoEdge<InfoNode>) e.getSourceTuple().get("edge_ref");
        return (float) edge.getWeight();
        
    }
    
}
