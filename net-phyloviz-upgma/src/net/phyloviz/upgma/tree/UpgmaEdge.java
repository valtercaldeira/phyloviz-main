/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.phyloviz.upgma.tree;

import java.util.ArrayList;
import net.phyloviz.core.data.InfoEdge;
import net.phyloviz.core.data.InfoNode;

/**
 *
 * @author Valter
 */
public class UpgmaEdge extends InfoEdge<UpgmaNode>{
    
    private UpgmaNode father = null;
    private UpgmaNode son = null;

    public UpgmaEdge(UpgmaNode u, UpgmaNode v) {
        super(u, v);
    }
    
    public UpgmaEdge(UpgmaNode u, UpgmaNode v, UpgmaNode f) {
        super(u, v);
        if(u.getID() == f.getID())son = v;
        else son = u;
        this.father = f;
    }

    public UpgmaNode getFather() {
        return father;
    }
    
    public UpgmaNode getSon() {
        return son;
    }
    
}
