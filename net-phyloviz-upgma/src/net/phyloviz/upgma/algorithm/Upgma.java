/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */
package net.phyloviz.upgma.algorithm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;
import net.phyloviz.core.data.AttributeMatrix;
import net.phyloviz.core.data.DistanceMatrix;
import net.phyloviz.core.data.DummyProfile;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.TaxaPair;
import net.phyloviz.upgma.AttributeMatrixUpgma;
import net.phyloviz.upgma.tree.UpgmaNode;

public class Upgma {

    private TreeMap<Integer, Double> distances = new TreeMap<Integer, Double>();;

    public Upgma() {
    }

    /**
     * Method that calculate all edges of Upgma
     *
     * @param matrix
     * @return edges
     */
    public AttributeMatrixUpgma getEdges(DistanceMatrix matrix) {
        //iniate AM
        AttributeMatrixUpgma am = new AttributeMatrixUpgma(matrix);

        int countDummy = -1;
        //execute until all profiles are connected
        while (!matrix.getAllTaxasPairs().isEmpty()) {

            //sort edges by distance
            TaxaPair<Profile> chosenEdge = (TaxaPair<Profile>) matrix.getFirstTaxaPairOrdered();

            //create the Dummy node
            double dist = matrix.getDistance(chosenEdge) / 2;
            Profile dp = new DummyProfile("dummy" + countDummy, countDummy );
            UpgmaNode dummy = new UpgmaNode(dp, dist);
            dummy.setID("dummy" + countDummy);
            dummy.setUID(countDummy);
            countDummy--;

            //Dist U-D
            double distUD = algoCalcs(chosenEdge.getU(), chosenEdge.getV(), matrix);
            //Dist V-D
            double distVD = algoCalcs(chosenEdge.getV(), chosenEdge.getU(), matrix);

            //Update Matrix
            dp = addAndRemove(dp, chosenEdge.getU(), chosenEdge.getV(), distUD, matrix);

            double newDist = 0;
            if (distances.get(chosenEdge.getU().getUID()) != null) {
                newDist += distances.get(chosenEdge.getU().getUID());
            }
            distances.put(dp.getUID(), distUD + newDist);

            // add edge U--D
                TaxaPair<Profile> newEdge = new TaxaPair(chosenEdge.getU(), dp);
                newEdge.setWeight(distUD);
                am.addEdge(newEdge,dp);
                

            // add edge V--D
                newEdge = new TaxaPair(chosenEdge.getV(), dp);
                newEdge.setWeight(distVD);
                am.addEdge(newEdge,dp);
                
        }

        return am;
    }

    /**
     * Calculate the distnace to be used for NJ, (distance of u)
     *
     * @param Profile u
     * @param Profile v
     * @param Matrix
     * @return double Dist U
     */
    private double algoCalcs(Profile u, Profile v, DistanceMatrix matrix) {
        // x + distU = y + distV
        // x + y = distUV
        double dist;
        double distU = (distances.containsKey(u.getUID())) ? distances.get(u.getUID()) : 0;
        double distV = (distances.containsKey(v.getUID())) ? distances.get(v.getUID()) : 0;
        double distUV = matrix.getDistance(new TaxaPair(u, v));
        dist = (distV + (distUV-distV-distU) - distU) / 2;
        return dist;
    }
    
     private Profile addAndRemove(Profile newNode, Profile oldNode1, Profile oldNode2, double dist, DistanceMatrix matrix) {
        matrix.incSize();
        Iterator i = matrix.getAllProfiles().iterator();
        while(i.hasNext()){
            Profile f = (Profile) i.next();
            if (f.getUID() == newNode.getUID() || f.getUID() == oldNode1.getUID() || f.getUID() == oldNode2.getUID()) {
                continue;
            }
            //get distance old-actual to get distance new-acrtual
            Double defNF = matrix.getDistance(new TaxaPair(oldNode1, f));
            defNF += matrix.getDistance(new TaxaPair(oldNode2, f));
            defNF /= 2;
            TaxaPair<Profile> tp = new TaxaPair<Profile>(newNode, f);
            matrix.putDistance(tp,defNF);  
        }
        remove(matrix,oldNode1);
        remove(matrix,oldNode2);
        return newNode;
    }
    
    private void remove(DistanceMatrix matrix, Profile n) {
        matrix.removeProfile(n);
    }

}
