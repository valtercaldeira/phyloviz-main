/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */
package net.phyloviz.upgma.run;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import javax.swing.SwingUtilities;
import net.phyloviz.core.data.AbstractProfile;
import net.phyloviz.core.data.AttributeMatrix;
import net.phyloviz.core.data.DataItem;
import net.phyloviz.core.data.DataModel;
import net.phyloviz.core.data.DataSet;
import net.phyloviz.core.data.DistanceMatrix;
import net.phyloviz.core.data.InfoEdge;
import net.phyloviz.core.data.Population;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.TaxaPair;
import net.phyloviz.core.data.TypingData;
import net.phyloviz.upgma.AttributeMatrixUpgma;
import net.phyloviz.upgma.UpgmaResult;
import net.phyloviz.upgma.algorithm.Upgma;
import net.phyloviz.upgma.tree.UpgmaEdge;
import net.phyloviz.upgma.tree.UpgmaNode;
import net.phyloviz.upgma.ui.OutputPanel;
import org.openide.nodes.Node;

public class UpgmaRunner implements Runnable {

    private Node n;
    private DataSet ds;
    private Population pop;
    private OutputPanel op;
    private DistanceMatrix matrix;
    private HashMap<String, String> st2cl;
    static int times = 0;

    public UpgmaRunner(Node n, OutputPanel op, DistanceMatrix dm) {
        this.n = n;
        this.op = op;
        ds = n.getParentNode().getLookup().lookup(DataSet.class);
        pop = ds.getLookup().lookup(Population.class);
        matrix = dm;
    }

    @Override
    public void run() {

        op.appendWithDate("Upgma started\nUpgma algorithm: computing groups...\n\n");
        op.flush();
        TypingData<? extends AbstractProfile> td = (TypingData<? extends AbstractProfile>) n.getLookup().lookup(TypingData.class);
        iniateDistanceMatrix();
        Upgma algorithm = new Upgma();
        AttributeMatrixUpgma am = algorithm.getEdges(matrix);

        // Integrate with isolate data, if it exists.
        if (pop != null) {
            op.appendWithDate("Upgma algorithm: integrating data...\n\n");
            op.flush();

            st2cl = new HashMap<String, String>();
            ArrayList<UpgmaEdge> a = (ArrayList<UpgmaEdge>) am.getEdges();
            Iterator<UpgmaEdge> igo;
            igo = a.iterator();
            while (igo.hasNext()) {
                InfoEdge<UpgmaNode> edge = igo.next();
                UpgmaNode u = edge.getU();
                st2cl.put(u.getID(), "1");
                UpgmaNode v = edge.getV();
                st2cl.put(v.getID(), "1");
            }
            pop.addColumn("Upgma[" + (times++) + "]", new DataModel.ColumnFiller() {

                @Override
                public String getValue(DataItem i) {
                    return st2cl.get(i.get(ds.getPopKey()));
                }
            });

            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    pop.tableModel().fireTableStructureChanged();
                }
            });
        }
        ArrayList<UpgmaEdge> a = (ArrayList<UpgmaEdge>) am.getEdges();
        Iterator<UpgmaEdge> iEg = a.iterator();
        op.append("#CC = " + am.size() + "\n\n");
        op.flush();
        while (iEg.hasNext()) {
            InfoEdge<UpgmaNode> edge = iEg.next();
            op.append("CC edge " + edge + " has " + edge.getWeight() + " Weight:\n");

            UpgmaNode st = (UpgmaNode) edge.getU();
            op.append("ST " + st.getID() + " (" + st.getFreq() + ") ");
            st = (UpgmaNode) edge.getV();
            op.append("ST " + st.getID() + " (" + st.getFreq() + ") ");

            if (edge.visible()) {
                op.append(edge.getU().getID() + " - " + edge.getV().getID());
                String einfo = am.info(edge);
                if (einfo != null) {
                    op.append(" (" + am.info(edge) + ")\n");
                } else {
                    op.append("\n");
                }
            } else {
                op.append(edge.getU().getID() + " - " + edge.getV().getID());
                String einfo = am.info(edge);
                if (einfo != null) {
                    op.append(" (" + am.info(edge) + ") -\n");
                } else {
                    op.append(" -\n");
                }
            }
            op.append("\n");
        }

        op.appendWithDate(
                "Upgma algorithm: computing statistics...\n");
        op.flush();

        op.appendWithDate(
                "Upgma algorithm: done.\n"+ am);
        op.flush();

        td.add(
                new UpgmaResult(am, op));
    }

    private void iniateDistanceMatrix() {
        matrix.setComparators(new EdegesComp(), new ProfileComp());
    }

    private class EdegesComp implements Comparator<TaxaPair<Profile>> {

        @Override
        public int compare(TaxaPair<Profile> edge1, TaxaPair<Profile> edge2) {
            double res =  matrix.getDistance(edge1) - matrix.getDistance(edge2);
            if(res < 0) return -1;
            if(res > 0) return 1;
            return 0;
        }

    }

    public class ProfileComp implements Comparator<Profile> {

        @Override
        public int compare(Profile n1, Profile n2) {
            return (int) (getS(n1) - getS(n2));
        }
    }

    private double getS(Profile p) {
        double s;
        s = matrix.getAllDistFromProfile(p.getUID());
        return s / (matrix.getSize() - 2);
    }
}
