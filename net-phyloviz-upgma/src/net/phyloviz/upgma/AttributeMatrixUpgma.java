/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.phyloviz.upgma;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeMap;
import net.phyloviz.core.data.AttributeMatrix;
import net.phyloviz.core.data.Cluster;
import net.phyloviz.core.data.DistanceMatrix;
import net.phyloviz.core.data.DummyProfile;
import net.phyloviz.core.data.InfoEdge;
import net.phyloviz.core.data.InfoNode;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.TaxaPair;
import net.phyloviz.core.util.NodeFactory;
import net.phyloviz.upgma.tree.UpgmaEdge;
import net.phyloviz.upgma.tree.UpgmaNode;
import net.phyloviz.upgma.ui.UpgmaNodeView;
import net.phyloviz.upgma.ui.OutputPanel;
import org.openide.nodes.AbstractNode;

/**
 *
 * @author Valter
 */
public class AttributeMatrixUpgma implements AttributeMatrix<UpgmaNode>{
    
    private TreeMap <Integer , ArrayList<UpgmaEdge>> sons = new TreeMap <Integer , ArrayList<UpgmaEdge>>();
    private DistanceMatrix dm;
    private ArrayList<Cluster> clusters = new ArrayList<Cluster>();

    public AttributeMatrixUpgma(DistanceMatrix dm) {
        this.dm = dm;
        Cluster c = new Cluster();
        c.setID(0);
        clusters.add(c);
    }
    
    public AttributeMatrixUpgma() {
        Cluster c = new Cluster();
        c.setID(0);
        clusters.add(c);
    }
    
    public void addEdge(TaxaPair<Profile> tp, Profile father) {
        UpgmaNode n1 = new UpgmaNode(tp.getU(), 0);
        UpgmaNode n2 = new UpgmaNode(tp.getV(), 0);
        UpgmaNode f = new UpgmaNode(father, 0);
        UpgmaEdge newEdge = new UpgmaEdge(n1,n2,f);
        newEdge.setWeight(tp.getWeight());
        clusters.get(0).add(newEdge);
        if(sons.get(father.getUID())== null){
            sons.put( father.getUID(), new ArrayList<UpgmaEdge>() );
        }
        sons.get(father.getUID()).add(newEdge);
    }
    
        public void addEdge(TaxaPair<Profile> tp, Profile father, int dummyNum) {
        UpgmaNode n1 = new UpgmaNode(tp.getU(), 0);
        UpgmaNode n2 = new UpgmaNode(tp.getV(), 0);
        UpgmaNode f = new UpgmaNode(father, 0);
        Profile dp = new DummyProfile("",dummyNum);
        UpgmaNode dummy = new UpgmaNode(dp, 1);
        double dN1 = 0, dN2 = 0;
        
        if(f.getUID() == n1.getUID()){
            dN1 = 1;
            dN2 = tp.getWeight();
        }else{
            dN1 = tp.getWeight();
            dN2 = 1;
        }
        
        UpgmaEdge newEdge = new UpgmaEdge(dummy,n1,f);
        newEdge.setWeight(dN1);
        clusters.get(0).add(newEdge);
        newEdge = new UpgmaEdge(dummy,n2,f);
        newEdge.setWeight(dN2);
        clusters.get(0).add(newEdge);
        
        
        newEdge = new UpgmaEdge(n1,n2,f);
        newEdge.setVisible(false);
        newEdge.setWeight(tp.getWeight());
        clusters.get(0).add(newEdge);
        if(sons.get(father.getUID())== null){
            sons.put( father.getUID(), new ArrayList<UpgmaEdge>() );
        }
        sons.get(father.getUID()).add(newEdge);
        
    }
    
    @Override
    public void addEdge(TaxaPair<Profile> tp) {
        UpgmaNode n1 = new UpgmaNode(tp.getU(), 0);
        UpgmaNode n2 = new UpgmaNode(tp.getV(), 0);
        UpgmaEdge newEdge = new UpgmaEdge(n1,n2);
        newEdge.setWeight(tp.getWeight());
        clusters.get(0).add(newEdge);
    }

    @Override
    public ArrayList<UpgmaEdge> getEdges() {
        return clusters.get(0).getEdges();
    }
    
    
    public int getDist(int id) {
        return clusters.get(0).getEdges().size();
    }

    @Override
    public int size() {
        return clusters.get(0).getEdges().size();
    }

    @Override
    public String info(InfoEdge<UpgmaNode> edge) {
        return edge.toString();
    }
    
    @Override
    public String toString() {
        String res ="";
        UpgmaEdge second = null;
        UpgmaEdge ue = null;
        Integer father;
        Iterator i = clusters.get(0).getEdges().iterator();
        while(i.hasNext()){
            ue = (UpgmaEdge) i.next();
            if(!ue.visible())continue;
            if(ue.getFather() != null) continue;
            if(second == null){
                second = ue;
                continue;
            }
            break;
        }
        
        res = "("+treeToString(ue.getFather().getUID())+");";
        return res;
    }

    private String treeToString(Integer father) {
        String res, resleft, resright = "";
        if(sons.get(father) == null || sons.get(father).isEmpty())return "";
        UpgmaEdge hu = sons.get(father).get(0);
        UpgmaEdge hv = sons.get(father).get(1);
        String sonLeft = treeToString(hu.getSon().getUID());
        String sonRight = treeToString(hv.getSon().getUID());
        if(sonLeft.equals(""))resleft = hu.getSon() +":"+hu.getWeight();
        else resleft = "(" + treeToString(hu.getSon().getUID())+")" + hu.getSon() +":"+hu.getWeight();
        
        if(sonRight.equals(""))resright = hv.getSon() +":"+hv.getWeight();
        else resright = "(" + treeToString(hv.getSon().getUID())+")" + hv.getSon() +":"+hv.getWeight();
        
        return resleft + "," + resright;
    }

    @Override
    public ArrayList<UpgmaEdge> getEdges(int idx) {
        return clusters.get(0).getEdges();
    }

    @Override
    public int groupsSize() {
        return 1;
    }

    @Override
    public Collection<UpgmaNode> getNodes(int idx) {
        return clusters.get(0).getSTs();
    }

    @Override
    public ArrayList<Cluster> getClusters() {
        return clusters;
    }

    @Override
    public DistanceMatrix getDistance() {
        return dm;
    }
    
    
    
}
