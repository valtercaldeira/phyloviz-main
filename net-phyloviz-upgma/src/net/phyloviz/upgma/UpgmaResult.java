/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.phyloviz.upgma;

import java.util.Collection;
import java.util.Map;
import net.phyloviz.core.data.AttributeMatrix;
import net.phyloviz.core.data.Cluster;
import net.phyloviz.core.util.NodeFactory;
import net.phyloviz.upgma.ui.OutputPanel;
import net.phyloviz.upgma.ui.UpgmaNodeView;
import org.openide.nodes.AbstractNode;

/**
 *
 * @author Valter
 */
public class UpgmaResult implements NodeFactory, Result {
    
	private OutputPanel op;
	private AttributeMatrixUpgma ad;

	public UpgmaResult( AttributeMatrixUpgma ad, OutputPanel op) {
		this.op = op;
		this.ad = ad;
	}
        
        public AttributeMatrixUpgma getAttributeMatrix(){
            return ad;
        }

	@Override
	public OutputPanel getPanel() {
		return op;
	}


	@Override
	public AbstractNode getNode() {
		return new UpgmaNodeView(this);
	}

	@Override
	public String toString() {
		return "upgma (" + ad.toString() + ")";
	}

    public void setEdgestats(Map<Integer, Double> result) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
