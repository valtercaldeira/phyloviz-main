/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tests;

import java.util.Comparator;
import java.util.TreeMap;
import net.phyloviz.core.data.Profile;

/**
 *
 * @author Valter
 */
public class ProfileTest implements Profile {
     private String[] seq;
    private String name;
    private int freq;
    private int w;
    private int id;
    
    
    public class LinesComp implements Comparator<TreeMap<Integer, Integer>> {

        public int compare(TreeMap<Integer, Integer> e1, TreeMap<Integer, Integer> e2) {
            int cf = e1.firstKey().compareTo(e2.firstKey());

            if (cf == 0) {
                cf = e1.get(e1.firstKey()).compareTo(e2.get(e2.firstKey()));
            }
            return cf;
        }
    }
    
    public ProfileTest(String name, String[] seq){
        this.name = name;
        this.id = Integer.parseInt(name);
        this.seq = seq;
        freq = 1;
    }

    @Override
    public int getUID() {
        return id;
    }

    @Override
    public String getID() {
        return name;
    }

    @Override
    public int profileLength() {
        return seq.length;
    }

    @Override
    public String getValue(int idx) {
        return seq[idx];
    }

    @Override
    public int getFreq() {
        return freq;
    }

    @Override
    public void setFreq(int freq) {
        this.freq = freq;
    }
    
    


    @Override
    public int length() {
        return seq.length;
    }

    @Override
    public int weight() {
        return w;
    }
    
    @Override
    public String toString() {
        return "ST" + name;
    }

    @Override
    public String get(int idx) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
