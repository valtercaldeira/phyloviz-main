/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tests;

import com.sun.org.apache.xalan.internal.xsltc.runtime.BasisLibrary;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import static junit.framework.Assert.assertEquals;
import net.phyloviz.core.data.AttributeMatrix;
import net.phyloviz.core.data.InfoEdge;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.SparseMatrix;
import net.phyloviz.core.data.TaxaPair;
import net.phyloviz.core.data.TypingData;
import net.phyloviz.upgma.algorithm.Upgma;
import net.phyloviz.upgma.tree.UpgmaNode;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Valter
 */
public class test {
    
    public test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        TypingData td = new TypingData(6);
        Collection<Profile> c = new ArrayList();
        Profile f1 = new ProfileTest("1", new String[]{"G", "C", "G", "G", "A", "C", "A", "A", "A"});
        Profile f2 = new ProfileTest("2", new String[]{"G", "A", "C", "G", "C", "C", "A", "A", "G"});
        Profile f3 = new ProfileTest("3", new String[]{"G", "A", "A", "A", "U", "C", "U", "A", "A"});
        Profile f4 = new ProfileTest("4", new String[]{"G", "A", "A", "A", "G", "C", "U", "A", "G"});
        c.add(f1);
        c.add(f2);
        c.add(f3);
        c.add(f4);
        sm = new SparseMatrix(c,new EdegesComp(), null);
    }
    
    @After
    public void tearDown() {
    }
    
    private SparseMatrix<ProfileTest> sm;
    
  

private class EdegesComp implements Comparator<TaxaPair<Profile>> {

        @Override
        public int compare(TaxaPair<Profile> edge1, TaxaPair<Profile> edge2) {
            double res =  sm.getDistance(edge1) - sm.getDistance(edge2);
            if(res < 0) return -1;
            if(res > 0) return 1;
            return 0;
        }

    }

    
    @Test
    public void testinggetSort() {
        //Testing
        ArrayList<TaxaPair<Profile>> a = sm.getAllTaxasPairs();
        int id1 = a.get(0).getU().getUID();
        int id2 = a.get(0).getV().getUID();
        assertEquals(id1, 3);
        assertEquals(id2, 4);
    }
    
    @Test
    public void testingalgo() {
        //Testing
        Upgma algorithm = new Upgma();
        AttributeMatrix am = algorithm.getEdges(sm);
        System.out.println(am.toString());
        Iterator i = am.getEdges().iterator();
        while(i.hasNext()){
            InfoEdge<UpgmaNode> tn = (InfoEdge<UpgmaNode>) i.next();
            if(tn.getU().getUID() == -3 && tn.getV().getUID() == -2){
                double teste = tn.getWeight();
                assertEquals(teste, 0.5, 0);
            }
        }
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
