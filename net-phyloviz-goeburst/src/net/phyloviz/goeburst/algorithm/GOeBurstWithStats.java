/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */
package net.phyloviz.goeburst.algorithm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.TreeMap;
import net.phyloviz.core.data.DisjointSet;
import net.phyloviz.core.data.DistanceMatrix;
import net.phyloviz.core.data.InfoEdge;
import net.phyloviz.core.data.InfoNode;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.TaxaPair;
import net.phyloviz.goeburst.cluster.GOeBurstClusterWithStats;
import net.phyloviz.goeburst.cluster.GOeBurstNodeExtended;

public class GOeBurstWithStats implements ClusteringMethod<GOeBurstClusterWithStats, GOeBurstNodeExtended> {

    private int level;
    private int maxStId;

    private class STLV {

        int[] lv = new int[GOeBurstClusterWithStats.MAXLV + 1];
    }
    private TreeMap<Integer, STLV> stLVs;

    public GOeBurstWithStats(int level) {
        this.level = level;
        this.stLVs = new TreeMap<Integer, STLV>();
    }

    ;
    @Override
    public Collection<GOeBurstClusterWithStats> getClustering(DistanceMatrix matrix, ArrayList<InfoEdge<GOeBurstNodeExtended>> edges) {

        
        Collection<GOeBurstClusterWithStats> clustering = getGroups(edges, matrix);

        // Update LVs for STs in each group and set group id.
        Iterator<GOeBurstClusterWithStats> gIter = clustering.iterator();
        int gid = 0;
        while (gIter.hasNext()) {
            GOeBurstClusterWithStats g = gIter.next();
            g.setID(gid++);
            g.updateVisibleEdges();
        }

        return clustering;
    }

    public int getSTxLV(InfoNode st, int lv) {
        if (lv > GOeBurstClusterWithStats.MAXLV || lv < 0) {
            level = GOeBurstClusterWithStats.MAXLV;
        }
        return stLVs.get(st.getUID()).lv[lv];
    }

    public ArrayList<InfoEdge<GOeBurstNodeExtended>> getEdges(DistanceMatrix m) {
        ArrayList<InfoEdge<GOeBurstNodeExtended>> edges = new ArrayList<InfoEdge<GOeBurstNodeExtended>>();
        maxStId = 0;

        Iterator<Profile> uIter = m.getAllProfiles().iterator();
        while (uIter.hasNext()) {
            Profile u = uIter.next();
            STLV uLV = stLVs.get(u.getUID());

            if (uLV == null) {
                uLV = new STLV();
                stLVs.put(u.getUID(), uLV);
            }

            maxStId = Math.max(maxStId, u.getUID());

            Iterator<Profile> vIter = m.getAllProfiles().iterator();
            while (vIter.hasNext()) {
                Profile v = vIter.next();

                int diff = m.getRatedDistance(new TaxaPair<Profile>(u,v));
                if (u != v && diff <= GOeBurstClusterWithStats.MAXLV) {
                    uLV.lv[diff - 1]++;
                } else {
                    uLV.lv[GOeBurstClusterWithStats.MAXLV]++;
                }

                if (u.getUID() < v.getUID() && diff <= level) {
                    edges.add(new InfoEdge<GOeBurstNodeExtended>(new GOeBurstNodeExtended(u), new GOeBurstNodeExtended(v)));
                }
            }
        }

        return edges;
    }

    private Collection<GOeBurstClusterWithStats> getGroups(ArrayList<InfoEdge<GOeBurstNodeExtended>> edges, DistanceMatrix matrix) {
        DisjointSet s = new DisjointSet(maxStId);
        TreeMap<Integer, GOeBurstClusterWithStats> groups = new TreeMap<Integer, GOeBurstClusterWithStats>();
        ArrayList<GOeBurstNodeExtended> listNodes = new ArrayList<GOeBurstNodeExtended>();
        GOeBurstNodeExtended newNode;
        
        Iterator<Profile> in = matrix.getAllProfiles().iterator();
        while (in.hasNext()) {
            newNode = new GOeBurstNodeExtended(in.next());
            listNodes.add(newNode);
        }
        
        Iterator<GOeBurstNodeExtended> it = listNodes.iterator();
        while (it.hasNext()) {
            GOeBurstNodeExtended n = it.next();
            n.updateLVsExtended(listNodes, matrix, matrix.maxLevel());
        }

        Iterator<InfoEdge<GOeBurstNodeExtended>> eIter = edges.iterator();
        while (eIter.hasNext()) {
            InfoEdge edge = eIter.next();
            s.unionSet(edge.getU().getUID(), edge.getV().getUID());
        }

        eIter = edges.iterator();
        while (eIter.hasNext()) {
            InfoEdge<GOeBurstNodeExtended> edge = eIter.next();
            GOeBurstNodeExtended gne = edge.getU();
            int pi = s.findSet(gne.getUID());
            GOeBurstClusterWithStats g = groups.get(pi);
            if (g == null) {
                g = new GOeBurstClusterWithStats(this, matrix);
                groups.put(pi, g);
            }
            GOeBurstNodeExtended gne2 = edge.getV();
            g.add(new InfoEdge<GOeBurstNodeExtended>(gne, gne2));
        }

        // Add singletons.
        Iterator<GOeBurstNodeExtended> stIter = listNodes.iterator();
        while (stIter.hasNext()) {
            GOeBurstNodeExtended st = (GOeBurstNodeExtended) (stIter.next());
            int pi = s.findSet(st.getUID());
            if (groups.get(pi) == null) {
                GOeBurstClusterWithStats g = new GOeBurstClusterWithStats(this, matrix);
                g.add(st);
                groups.put(pi, g);
            }
            ((GOeBurstClusterWithStats) groups.get(pi)).updateMaxLVs(st);
        }

        ArrayList<GOeBurstClusterWithStats> gList = new ArrayList<GOeBurstClusterWithStats>(groups.values());
        Collections.sort(gList);

        return gList;
    }
}
