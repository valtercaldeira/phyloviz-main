/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.phyloviz.goeburst;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeMap;
import net.phyloviz.core.data.AttributeMatrix;
import net.phyloviz.core.data.Cluster;
import net.phyloviz.core.data.DistanceMatrix;
import net.phyloviz.core.data.InfoEdge;
import net.phyloviz.core.data.InfoNode;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.TaxaPair;
import net.phyloviz.core.util.NodeFactory;
import net.phyloviz.goeburst.cluster.GOeBurstClusterWithStats;
import net.phyloviz.goeburst.tree.GOeBurstNode;
import org.openide.nodes.AbstractNode;

/**
 *
 * @author Valter
 */
public class AttributeMatrixGOeBurst implements AttributeMatrix<GOeBurstNode>{
    
    private TreeMap <Integer , ArrayList<GOeBurstNode>> sons = new TreeMap <Integer , ArrayList<GOeBurstNode>>();
    private DistanceMatrix dm;
    private Collection<GOeBurstClusterWithStats> c;
    private Collection<InfoEdge<GOeBurstNode>> e;

    public AttributeMatrixGOeBurst(DistanceMatrix dm) {
        this.dm = dm;
        Cluster c = new Cluster();
        c.setID(0);
    }
    
    public AttributeMatrixGOeBurst() {
        Cluster c = new Cluster();
        c.setID(0);
    }
    
    public AttributeMatrixGOeBurst(Collection<GOeBurstClusterWithStats> c, DistanceMatrix dm) {
        this.c = c;
        this.dm = dm;
    }
    
    public AttributeMatrixGOeBurst(ArrayList<InfoEdge<GOeBurstNode>> edges) {
        this.e = edges;
    }
   

    @Override
    public int size() {
        return c.size();
    }

    @Override
    public String info(InfoEdge<GOeBurstNode> edge) {
        return edge.toString();
    }

    @Override
    public int groupsSize() {
        return c.size();
    }

    @Override
    public Collection<GOeBurstNode> getNodes(int idx) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    public Collection<GOeBurstClusterWithStats> GOeBurstClusters() {
        return c;
    }

    @Override
    public DistanceMatrix getDistance() {
        return dm;
    }

    @Override
    public void addEdge(TaxaPair<Profile> tp) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<? extends InfoEdge<GOeBurstNode>> getEdges() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<? extends InfoEdge<GOeBurstNode>> getEdges(int idx) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Cluster> getClusters() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
