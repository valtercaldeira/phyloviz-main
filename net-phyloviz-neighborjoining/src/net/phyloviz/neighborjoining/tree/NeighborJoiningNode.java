/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */
package net.phyloviz.neighborjoining.tree;


import net.phyloviz.core.data.DistanceMatrix;
import net.phyloviz.core.data.DummyProfile;
import net.phyloviz.core.data.InfoNode;
import net.phyloviz.core.data.Profile;

public class NeighborJoiningNode implements InfoNode, Comparable<NeighborJoiningNode> {
    protected Profile p;
    protected double S;
    private int uid;
    private String id;
    private boolean vis = true;

    public NeighborJoiningNode(Profile p, DistanceMatrix m) {
        this.p = p;
        this.uid = p.getUID();
        this.id = p.getID();
        updateValues(m);
    }
    
    public NeighborJoiningNode() {
        this.p = null;
    }

    public void updateValues(DistanceMatrix m) {
        if(p.getClass().isAssignableFrom(DummyProfile.class))return;
        S = m.getAllDistFromProfile(p.getUID());
        S = S / (m.getSize() - 2);
    }

    public double getS() {
        return S;
    }

    public Profile getProfile() {
        return p;
    }

    @Override
    public int getUID() {
        if(p == null)return uid;
        return p.getUID();
    }
    
    public void setUID(int id) {
        uid = id;
    }
    
    public void setID(String name) {
        id = name;
    }


    @Override
    public String getID() {
        if(p == null)return id;
        return p.getID();
    }

    @Override
    public int profileLength() {
        if(p == null)return 1;
        return p.profileLength();
    }

    @Override
    public String getValue(int idx) {
        if(p == null)return "0";
        return p.getValue(idx);

    }

    @Override
    public int getFreq() {
        if(p == null)return 0;
        return p.getFreq();
    }

    @Override
    public void setFreq(int freq) {
        p.setFreq(freq);
    }

    @Override
    public String get(int idx) {
        return p.get(idx);
    }

    @Override
    public int weight() {
        if(p == null)return 0;
        return p.weight();
    }

    @Override
    public int compareTo(NeighborJoiningNode o) {
        return p.getUID() - o.getUID();
    }

    public boolean equals(NeighborJoiningNode o) {
        return this.compareTo(o) == 0;
    }

    @Override
    public String toString() {
        return p.toString();
    }

    @Override
    public void setVisible(boolean vis) {
        this.vis = vis;
    }

    @Override
    public boolean visible() {
        return vis;
    }
}
