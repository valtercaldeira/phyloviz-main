/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */
package net.phyloviz.neighborjoining.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import net.phyloviz.core.data.AttributeMatrix;
import net.phyloviz.core.data.DistanceMatrix;
import net.phyloviz.core.data.DummyProfile;
import net.phyloviz.core.data.InfoEdge;
import net.phyloviz.core.data.InfoNode;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.TaxaPair;
import net.phyloviz.neighborjoining.AttributeMatrixNJ;
import net.phyloviz.neighborjoining.tree.NeighborJoiningNode;

public class NeighborJoining {

    private DistanceMatrix matrix;

    public NeighborJoining() {
    }

    private AttributeMatrix<NeighborJoiningNode> am;

    /**
     * Method that calculate all edges of NJ
     *
     * @param DistanceMatrix
     * @retur edges
     */
    public AttributeMatrix<NeighborJoiningNode> getEdges(DistanceMatrix m) {
        //iniate AM
        this.matrix = m;
        am = new AttributeMatrixNJ(matrix);
        int countDummy = -1;
        //execute until all profiles are connected
        while (matrix.getSizeOfAllTaxasPairs() != 0) {
            iniateDistanceMatrix();
            //sort edges by NJ Algorithm, using MAB = DAB - A.S() - B.S()
            TaxaPair<Profile> chosenEdge = (TaxaPair<Profile>) matrix.getFirstTaxaPairOrdered();

            //create the Dummy node
            Profile dp = new DummyProfile("dummy" + countDummy, countDummy);
            NeighborJoiningNode dummy = new NeighborJoiningNode(dp, matrix);
            dummy.setID("dummy" + countDummy);
            dummy.setUID(countDummy);
            dummy.setFreq(1);
            countDummy--;

            // dist form dummy to the two nodes, by: DAB/2 +(A.S() - B.S())/2
            //Dist U-D
            double distUD = algoCalcs(chosenEdge.getU(), chosenEdge.getV(), matrix);
            //Dist V-D
            double distVD = algoCalcs(chosenEdge.getV(), chosenEdge.getU(), matrix);

            //Update Matrix
            if (distUD == 0) {
                dp = addAndRemove(dp, chosenEdge.getU(), distUD, matrix);
                dp = addAndRemove(dp, chosenEdge.getV(), distVD, matrix);
            } else {
                dp = addAndRemove(dp, chosenEdge.getV(), distVD, matrix);
                dp = addAndRemove(dp, chosenEdge.getU(), distUD, matrix);
            }

            // add edge U--D
            if (chosenEdge.getU().getUID() != dp.getUID()) {
                TaxaPair<Profile> newEdge = new TaxaPair(chosenEdge.getU(), dp);
                newEdge.setWeight(distUD);
                am.addEdge(newEdge);
            }

            // add edge V--D
            if (chosenEdge.getV().getUID() != dp.getUID()) {
                TaxaPair<Profile> newEdge = newEdge = new TaxaPair(chosenEdge.getV(), dp);
                newEdge.setWeight(distVD);
                am.addEdge(newEdge);
            }

        }

        return am;
    }

    /**
     * Calculate the distnace to be used for NJ, (distance of u)
     *
     * @param Profile u
     * @param Profile v
     * @param Matrix
     * @return double Dist U
     */
    private double algoCalcs(Profile u, Profile v, DistanceMatrix matrix) {
        double dist = matrix.getDistance(new TaxaPair(u, v)) / 2;
        double allDist = matrix.getAllDistFromProfile(u.getUID());
        double size = matrix.getSize() - 2;
        if (size == 0) {
            size = 1;
        }
        double uS = allDist / size;
        dist = matrix.getDistance(new TaxaPair(u, v)) / 2;
        allDist = matrix.getAllDistFromProfile(v.getUID());
        double vS = allDist / size;
        dist += ((uS - vS) / 2);
        return dist;
    }

    private Profile addAndRemove(Profile newNode, Profile oldNode, double dist, DistanceMatrix matrix) {
        if (dist == 0) {
            return oldNode;
        }
        if(newNode.getClass().isAssignableFrom(DummyProfile.class))matrix.incSize();

        Iterator edges = matrix.getAllTaxasPairs().iterator();
        while (edges.hasNext()) {
            TaxaPair tp = (TaxaPair) edges.next();
            if (tp.getU().getUID() != oldNode.getUID() && tp.getV().getUID() != oldNode.getUID()) {
                continue;
            }
            Double def = tp.getWeight();
            def = def - dist;
            if(def == 0)continue;
            Profile f;
            if (tp.getU().getUID() == oldNode.getUID()) {
                f = tp.getV();
            } else {
                f = tp.getU();
            }
            tp = new TaxaPair<Profile>(newNode, f);
            if(newNode.getUID() != f.getUID()) matrix.putDistance(tp, def);
        }

        matrix.removeProfile(oldNode);
        return newNode;
    }

    private void iniateDistanceMatrix() {
        matrix.setComparators(new EdegesComp(matrix), new ProfileComp(matrix));
    }

    private class EdegesComp implements Comparator<TaxaPair<Profile>> {

        private DistanceMatrix m;

        private EdegesComp(DistanceMatrix matrix) {
            this.m = matrix;
        }

        @Override
        public int compare(TaxaPair<Profile> edge1, TaxaPair<Profile> edge2) {
            double m1 = m.getDistance(edge1) - getS(edge1.getU()) - getS(edge1.getV());
            double m2 = m.getDistance(edge2) - getS(edge2.getU()) - getS(edge2.getV());
            double res = m1 - m2;
            res *= 100;
            return (int) res;
        }

        private double getS(Profile p) {
            double s;
            s = m.getAllDistFromProfile(p.getUID());
            s = s / (m.getSize() - 2);
            return s;
        }

    }

    public class ProfileComp implements Comparator<Profile> {

        private DistanceMatrix m;

        private ProfileComp(DistanceMatrix matrix) {
            this.m = matrix;
        }

        @Override
        public int compare(Profile n1, Profile n2) {
            return (int) (getS(n1) - getS(n2));
        }

        private double getS(Profile p) {
            double s;
            s = m.getAllDistFromProfile(p.getUID());
            s = s / (m.getSize() - 2);
            return s;
        }
    }

}
