/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */
package net.phyloviz.neighborjoining.run;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import javax.swing.SwingUtilities;
import net.phyloviz.core.data.AbstractProfile;
import net.phyloviz.core.data.AttributeMatrix;
import net.phyloviz.core.data.DataItem;
import net.phyloviz.core.data.DataModel;
import net.phyloviz.core.data.DataSet;
import net.phyloviz.core.data.DistanceMatrix;
import net.phyloviz.core.data.InfoEdge;
import net.phyloviz.core.data.InfoNode;
import net.phyloviz.core.data.Population;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.SparseMatrix;
import net.phyloviz.core.data.TaxaPair;
import net.phyloviz.core.data.TypingData;
import net.phyloviz.neighborjoining.AttributeMatrixNJ;
import net.phyloviz.neighborjoining.NeighborJoiningResult;
import net.phyloviz.neighborjoining.algorithm.NeighborJoining;
import net.phyloviz.neighborjoining.tree.NeighborJoiningNode;
import net.phyloviz.neighborjoining.ui.OutputPanel;
import org.openide.nodes.Node;

public class NeighborJoiningRunner implements Runnable {

    private Node n;
    private DataSet ds;
    private Population pop;
    private OutputPanel op;
    private DistanceMatrix matrix;
    private HashMap<String, String> st2cl;
    static int times = 0;

    public NeighborJoiningRunner(Node n, OutputPanel op, DistanceMatrix dm) {
        this.n = n;
        this.op = op;
        ds = n.getParentNode().getLookup().lookup(DataSet.class);
        pop = ds.getLookup().lookup(Population.class);
        matrix = dm;
    }

    @Override
    public void run() {

        op.appendWithDate("NeighborJoining started\nNeighborJoining algorithm: computing groups...\n\n");
        op.flush();
        TypingData<? extends AbstractProfile> td = (TypingData<? extends AbstractProfile>) n.getLookup().lookup(TypingData.class);
        NeighborJoining algorithm = new NeighborJoining();
        AttributeMatrix<NeighborJoiningNode> am = algorithm.getEdges(matrix);

        // Integrate with isolate data, if it exists.
        if (pop != null) {
            op.appendWithDate("NeighborJoining algorithm: integrating data...\n\n");
            op.flush();

            st2cl = new HashMap<String, String>();
            ArrayList<InfoEdge<NeighborJoiningNode>> a = (ArrayList<InfoEdge<NeighborJoiningNode>>) am.getEdges();
            Iterator<InfoEdge<NeighborJoiningNode>> igo = a.iterator();
            while (igo.hasNext()) {
                InfoEdge<NeighborJoiningNode> edge = igo.next();
                NeighborJoiningNode u = edge.getU();
                st2cl.put(u.getID(), "1");
                NeighborJoiningNode v = edge.getV();
                st2cl.put(v.getID(), "1");
            }
            pop.addColumn("NeighborJoining[" + (times++) + "]", new DataModel.ColumnFiller() {

                @Override
                public String getValue(DataItem i) {
                    return st2cl.get(i.get(ds.getPopKey()));
                }
            });

            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    pop.tableModel().fireTableStructureChanged();
                }
            });
        }
        ArrayList<InfoEdge<NeighborJoiningNode>> a = (ArrayList<InfoEdge<NeighborJoiningNode>>) am.getEdges();
        Iterator<InfoEdge<NeighborJoiningNode>> iEg = a.iterator();
        op.append("#CC = " + am.size() + "\n\n");
        op.flush();
        while (iEg.hasNext()) {
            InfoEdge<NeighborJoiningNode> edge = iEg.next();
            op.append("CC edge " + edge + " has " + edge.getWeight() + " Weight:\n");

            NeighborJoiningNode st = (NeighborJoiningNode) edge.getU();
            op.append("ST " + st.getID() + " (" + st.getFreq() + ") ");
            st = (NeighborJoiningNode) edge.getV();
            op.append("ST " + st.getID() + " (" + st.getFreq() + ") ");

            if (edge.visible()) {
                op.append(edge.getU().getID() + " - " + edge.getV().getID());
                String einfo = am.info(edge);
                if (einfo != null) {
                    op.append(" (" + am.info(edge) + ")\n");
                } else {
                    op.append("\n");
                }
            } else {
                op.append(edge.getU().getID() + " - " + edge.getV().getID());
                String einfo = am.info(edge);
                if (einfo != null) {
                    op.append(" (" + am.info(edge) + ") -\n");
                } else {
                    op.append(" -\n");
                }
            }
            op.append("\n");
        }

        op.appendWithDate(
                "NeighborJoining algorithm: computing statistics...\n");
        op.flush();

        op.appendWithDate(
                "NeighborJoining algorithm: done.\n");
        op.flush();

        td.add(
                new NeighborJoiningResult(am, op));
    }

}
