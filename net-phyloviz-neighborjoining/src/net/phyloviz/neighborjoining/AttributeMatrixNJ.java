/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.phyloviz.neighborjoining;

import java.util.ArrayList;
import java.util.Collection;
import net.phyloviz.core.data.AttributeMatrix;
import net.phyloviz.core.data.Cluster;
import net.phyloviz.core.data.DistanceMatrix;
import net.phyloviz.core.data.DummyProfile;
import net.phyloviz.core.data.InfoEdge;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.TaxaPair;
import net.phyloviz.neighborjoining.tree.NeighborJoiningNode;



/**
 *
 * @author Valter
 */
public class AttributeMatrixNJ implements AttributeMatrix<NeighborJoiningNode>{

    private DistanceMatrix dm;
    private ArrayList<Cluster> cl = new  ArrayList<Cluster>();

    public AttributeMatrixNJ(DistanceMatrix dm) {
        this.dm = dm;
        cl.add(0, new Cluster());
    }
    
    @Override
    public void addEdge(TaxaPair<Profile> tp) {
        NeighborJoiningNode n1 = new NeighborJoiningNode(tp.getU(), dm);
        NeighborJoiningNode n2 = new NeighborJoiningNode(tp.getV(), dm);
        if(n1.getProfile().getClass().isAssignableFrom(DummyProfile.class))n1.setVisible(false);
        if(n2.getProfile().getClass().isAssignableFrom(DummyProfile.class))n2.setVisible(false);
        InfoEdge newEdge = new InfoEdge<NeighborJoiningNode>(n1,n2);
        newEdge.setWeight(tp.getWeight());
        cl.get(0).add(newEdge);
    }

    @Override
    public ArrayList<InfoEdge<NeighborJoiningNode>> getEdges() {
        return cl.get(0).getEdges();
    }

    @Override
    public int size() {
        return cl.get(0).getEdges().size();
    }

    @Override
    public String info(InfoEdge<NeighborJoiningNode> edge) {
        return edge.toString();
    }

    @Override
    public ArrayList<? extends InfoEdge<NeighborJoiningNode>> getEdges(int idx) {
        return cl.get(idx).getEdges();
    }

    @Override
    public int groupsSize() {
        return 1;
    }

    @Override
    public Collection<NeighborJoiningNode> getNodes(int idx) {
        return cl.get(0).getSTs();
    }

    @Override
    public ArrayList<Cluster> getClusters() {
        return cl;
    }

    @Override
    public DistanceMatrix getDistance() {
        return dm;
    }
    
    public void remove(NeighborJoiningNode id){
        cl.get(0).treeNodes.remove(id);
    }
    
}
