/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import static junit.framework.Assert.assertEquals;
import net.phyloviz.core.data.AttributeMatrix;
import net.phyloviz.core.data.InfoEdge;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.SparseMatrix;
import net.phyloviz.core.data.TaxaPair;
import net.phyloviz.core.data.TypingData;
import net.phyloviz.neighborjoining.algorithm.NeighborJoining;
import net.phyloviz.neighborjoining.tree.NeighborJoiningNode;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Valter
 */
public class tests {
    
    public tests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        TypingData td = new TypingData(6);
        Collection<Profile> c = new ArrayList();
        Profile f1 = new ProfileTest("1", new String[]{"2", "3", "4", "3", "8", "4", "6"});
        Profile f2 = new ProfileTest("2", new String[]{"2", "3", "4", "53", "8", "4", "6"});
        Profile f3 = new ProfileTest("3", new String[]{"2", "3", "4", "53", "9", "4", "6"});
        Profile f4 = new ProfileTest("4", new String[]{"2", "3", "4", "3", "8", "110", "20"});
        Profile f5 = new ProfileTest("5", new String[]{"2", "3", "19", "3", "8", "4", "6"});
        c.add(f1);
        c.add(f2);
        c.add(f3);
        c.add(f4);
        c.add(f5);
        sm = new SparseMatrix(c, new EdegesComp(), new ProfileComp());
        sm.setComparators( new EdegesComp(), new ProfileComp());
    }
    
    @After
    public void tearDown() {
    }
    
    
    private SparseMatrix<ProfileTest> sm;
    
  

   private class EdegesComp implements Comparator<TaxaPair<Profile>> {

        @Override
        public int compare(TaxaPair<Profile> edge1, TaxaPair<Profile> edge2) {
            double m1 = sm.getDistance(edge1) - getS(edge1.getU()) - getS(edge1.getV());
            double m2 = sm.getDistance(edge2) - getS(edge2.getU()) - getS(edge2.getV());
            double res = m1 - m2;
            if (res < 0) {
                return -1;
            }
            if (res > 0) {
                return 1;
            }
            return 0;
        }

    }

    public class ProfileComp implements Comparator<Profile> {

        @Override
        public int compare(Profile n1, Profile n2) {
            return (int) (getS(n1) - getS(n2));
        }
    }

    private double getS(Profile p) {
        double s;
        s = sm.getAllDistFromProfile(p.getUID());
        return s / (sm.getSize() - 2);
    }
    
    @Test
    public void testinggetSort() {
        //Testing
        ArrayList<TaxaPair<Profile>> a = sm.getAllTaxasPairs();
        int id1 = a.get(0).getU().getUID();
        int id2 = a.get(0).getV().getUID();
        assertEquals(id1, 2);
        assertEquals(id2, 3);
    }
    
    @Test
    public void testingAlgorithm() {
        //Testing
        NeighborJoining algorithm = new NeighborJoining();
        AttributeMatrix am = algorithm.getEdges(sm);
        Iterator i = am.getEdges().iterator();
        while(i.hasNext()){
            InfoEdge<NeighborJoiningNode> tn = (InfoEdge<NeighborJoiningNode>) i.next();
            if(tn.getU().getUID() == 2 && tn.getV().getUID() == 3){
                double teste = tn.getWeight();
                assertEquals(teste, 1.0);
            }
        }
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
