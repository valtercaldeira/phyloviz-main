/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.phyloviz.core.data;

/**
 *
 * @author Valter
 */
public interface InfoNode {
    
    public int getUID();
    public String getID();
    public int profileLength();
    public String getValue(int idx);
    public int getFreq();
    public Profile getProfile();
    public void setFreq(int freq);
    public String get(int idx);
    public int weight();
    
    public void setVisible(boolean vis);

    public boolean visible();
    
}
