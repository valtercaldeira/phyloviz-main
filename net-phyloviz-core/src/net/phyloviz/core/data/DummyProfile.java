/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.phyloviz.core.data;

/**
 *
 * @author Valter
 */
public class DummyProfile extends AbstractProfile implements Comparable<Profile>{
    
    private int length = 0;
    private int freq = 0;
    private String name;
    
    public DummyProfile(String name, int id){
        this.name = name;
        this.uid = id;
        this.id = "";
    }
    
    public DummyProfile(){
        this.name = "";
        this.uid = -1;
        this.id = "";
    }
    
    
    
    @Override
    public int profileLength() {
        return length;
    }

    @Override
    public String getValue(int idx) {
        return name;
    }
    
    @Override
    public String toString() {
        return name;
    }

    @Override
    public int getFreq() {
        return freq;
    }

    @Override
    public void setFreq(int freq) {
        this.freq = freq;
    }

    @Override
    public int compareTo(Profile o) {
        return this.uid - o.getUID();
    }
    
}
