/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */
package net.phyloviz.core.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author Valter
 * @param <T>
 */
public class SparseMatrix<T extends Profile> extends AbstractDistanceMatrix<T> {

    private Comparator<TaxaPair<Profile>> taxasComp;
    private Comparator<Profile> profileComp;
    private int maxLevel = 3;

    //todos os edges e as suas distancias
    //private TreeMap<TaxaPair<Profile>, Double> distances = new TreeMap<TaxaPair<Profile>, Double>(new TComp());
    private TreeSet<TaxaPair<Profile>> edges = new TreeSet<TaxaPair<Profile>>(new TComp());

    public SparseMatrix(TypingData<? extends Profile> td) {
        super(td);
        iniate();
    }

    //Just for DEBUG
    public SparseMatrix(Collection<T> c, Comparator<TaxaPair<Profile>> compTaxas, Comparator<Profile> compProfile) {
        super(c.size());
        iniate();
        setComparators(compTaxas, compProfile);
        Iterator i1 = c.iterator();
        while (i1.hasNext()) {
            Profile f1 = (Profile) i1.next();
            Size++;
            Iterator i2 = c.iterator();
            while (i2.hasNext()) {
                Profile f2 = (Profile) i2.next();
                if (f1.getUID() <= f2.getUID()) {
                    continue;
                }
                double def = getDifference(f1, f2);
                TaxaPair<Profile> tp = new TaxaPair<Profile>(f1, f2, def);
                edges.add(tp);
                //distances.put(tp, def);
            }
        }
    }

    public SparseMatrix(TypingData<? extends Profile> td, int maxLevel) {
        super(td);
        this.maxLevel = maxLevel;
        iniate();
    }

    public int maxLevel() {
        return maxLevel;
    }

    @Override
    public void setComparators(Comparator<TaxaPair<Profile>> compTaxas, Comparator<Profile> compProfile) {
        profileComp = compProfile;
        taxasComp = compTaxas;
    }

    @Override
    public int compareTaxasPairs(TaxaPair<Profile> e, TaxaPair<Profile> f) {
        return taxasComp.compare(e, f);
    }

    @Override
    public Collection<Profile> getAllProfiles() {
        return td.getItems();
    }

    @Override
    public String toString() {
        return "SparseMatrix";
    }

    @Override
    public int compareProfiles(Profile e, Profile f) {
        return profileComp.compare(e, f);
    }

    @Override
    public double getAllDistFromProfile(int index) {
        Double res = 0.0;
        Iterator i = edges.iterator();
        while (i.hasNext()) {
            TaxaPair<Profile> tp = (TaxaPair<Profile>) i.next();
            if (tp.getU().getUID() == index || tp.getV().getUID() == index) {
                res += tp.getWeight();
            }
        }
        /*for (Entry<TaxaPair<Profile>, Double> entry : distances.entrySet()) {
         TaxaPair<Profile> tp = entry.getKey();
         if (tp.getU().getUID() == index || tp.getV().getUID() == index) {
         res += (Double) entry.getValue();
         }

         }*/
        return res;
    }

    @Override
    public ArrayList<TaxaPair<Profile>> getAllTaxasPairs() {
        ArrayList<TaxaPair<Profile>> a = new ArrayList<TaxaPair<Profile>>();
        Iterator i = edges.iterator();
        while (i.hasNext()) {
            a.add((TaxaPair<Profile>) i.next());
        }
        return a;
    }

    @Override
    public TaxaPair<Profile> getFirstTaxaPairOrdered() {
        TaxaPair result = null;
        TaxaPair tp;
        double value;

        Iterator i = edges.iterator();
        while (i.hasNext()) {
            tp = (TaxaPair) i.next();
            if (tp.getU().getUID() == tp.getV().getUID()) {
                continue;
            }
            if (result == null) {
                result = tp;
                value = tp.getWeight();
            } else {
                if (taxasComp.compare(tp, result) < 0) {
                    result = tp;
                    value = tp.getWeight();
                }
            }
        }

        /*for (Entry<TaxaPair<Profile>, Double> e : distances.entrySet()) {
         tp = e.getKey();
         if(tp.getU().getUID() == tp.getV().getUID())continue;
         if(result == null){
         result = tp;
         value = e.getValue();
         }else{
         if(taxasComp.compare(tp,result )<0){
         result = tp;
         value = e.getValue();
         }
         }
         }*/
        return result;
    }

    @Override
    public int getSizeOfAllTaxasPairs() {
        return edges.size();
    }

    @Override
    public int compare(Profile px, Profile py) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int level(Profile px, Profile py) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String info(Profile px, Profile py) {
        return px.toString() + py.toString();
    }

    @Override
    public boolean configurable() {
        return false;
    }

    @Override
    public void configure() {

    }

    @Override
    public Comparator<TaxaPair<Profile>> getTaxasPairComparator() {
        return taxasComp;
    }

    @Override
    public void update(TaxaPair<Profile> edge, double dist) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void putDistance(TaxaPair<Profile> tp, Double def) {
        //distances.put(tp, def);
        tp.setWeight(def);
        edges.add(tp);
    }

    @Override
    public void removeProfile(Profile f) {
        Size--;
        int idx = f.getUID();
        TreeSet<TaxaPair<Profile>> tempMap = new TreeSet<TaxaPair<Profile>>();
        //selecting edge to be removed
        Iterator i = edges.iterator();
        while (i.hasNext()) {
            TaxaPair<Profile> tp = (TaxaPair<Profile>) i.next();
            if (tp.getU().getUID() == idx || tp.getV().getUID() == idx) {
                tempMap.add(tp);
            }
        }
        /*for (Entry<TaxaPair<Profile>, Double> e : distances.entrySet()) {
         TaxaPair<Profile> tp = e.getKey();
         if (tp.getU().getUID() == idx || tp.getV().getUID() == idx) {
         tempMap.put(e.getKey(), e.getValue());
         }
         }*/
        //removing edges
        Iterator t = tempMap.iterator();
        while (t.hasNext()) {
            TaxaPair<Profile> tp = (TaxaPair<Profile>) t.next();
            edges.remove(tp);
        }
        /*for (Entry<TaxaPair<Profile>, Double> e : tempMap.entrySet()) {
         distances.remove(e.getKey());
         }*/
    }

    @Override
    public int compare(TaxaPair<T> ex, TaxaPair<T> ey) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int level(TaxaPair<T> e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String info(TaxaPair<T> e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Comparator<T> getProfileComparator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Comparator<TaxaPair<T>> getEdgeComparator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Double> getDistsFromProfile(int idx) {
        ArrayList<Double> res = new ArrayList<Double>();

        Iterator i = edges.iterator();
        while (i.hasNext()) {
            TaxaPair tp = (TaxaPair) i.next();
            if (tp.getU().getUID() == idx || tp.getV().getUID() == idx) {
                res.add(tp.getWeight());
            }
        }

        /*for (Entry<TaxaPair<Profile>, Double> e : distances.entrySet()) {
         TaxaPair<Profile> tp = e.getKey();
         if (tp.getU().getUID() == idx || tp.getV().getUID() == idx) {
         res.add(e.getValue());
         }
         }*/
        return res;
    }

    @Override
    public void incSize() {
        Size++;
    }

    public class TComp implements Comparator<TaxaPair<Profile>> {

        @Override
        public int compare(TaxaPair<Profile> e1, TaxaPair<Profile> e2) {
            if ((e1.getU().getUID() == e2.getU().getUID()) && (e1.getV().getUID() == e2.getV().getUID())) {
                return 0;
            }
            if ((e1.getU().getUID() == e2.getV().getUID()) && (e1.getV().getUID() == e2.getU().getUID())) {
                return 0;
            }

            int cf = e1.getU().getUID() - e2.getU().getUID();

            if (cf == 0) {
                cf = e1.getV().getUID() - e2.getV().getUID();
            }
            return cf;
        }
    }

    private void iniate() {
        Iterator i1 = td.getItems().iterator();
        while (i1.hasNext()) {
            Profile f1 = (Profile) i1.next();
            Size++;
            Iterator i2 = td.getItems().iterator();
            while (i2.hasNext()) {
                Profile f2 = (Profile) i2.next();
                if (f1.getUID() <= f2.getUID()) {
                    continue;
                }

                double def = getDifference(f1, f2);
                TaxaPair<Profile> tp = new TaxaPair<Profile>(f1, f2, def);
                edges.add(tp);
                //distances.put(tp, def);
            }
        }
    }

    @Override
    public int getSize() {
        return Size;
    }

    @Override
    public ArrayList<TaxaPair<Profile>> getTaxasPairs(int level) {
        ArrayList<TaxaPair<Profile>> tps = new ArrayList<TaxaPair<Profile>>();

        Iterator i = edges.iterator();
        while (i.hasNext()) {
            TaxaPair tp = (TaxaPair) i.next();
            double value = tp.getWeight();
            if (value <= level) {
                tps.add(tp);
            }
        }
        /*for (Map.Entry<TaxaPair<Profile>, Double> entry : distances.entrySet()) {

         Double value = entry.getValue();
         if (value <= level) {
         tp.add(entry.getKey());
         }
         }*/

        return tps;
    }

    @Override
    public double getDistance(TaxaPair<Profile> e) {
        if (e.getU().getUID() == e.getV().getUID()) {
            return 0;
        }
        Iterator i = edges.iterator();
        while (i.hasNext()) {
            TaxaPair tp = (TaxaPair) i.next();
            if ((tp.getU().getUID() == e.getU().getUID() && tp.getV().getUID() == e.getV().getUID()) || (tp.getU().getUID() == e.getV().getUID() && tp.getV().getUID() == e.getU().getUID())) {
                return tp.getWeight();
            }
        }
        return -1;
        //return distances.get(e);
    }

    @Override
    public int getRatedDistance(TaxaPair<Profile> e) {
        if (e.getU().getUID() == e.getV().getUID()) {
            return 0;
        }
        double dist = -1;
        Iterator i = edges.iterator();
        while (i.hasNext()) {
            TaxaPair tp = (TaxaPair) i.next();
            if ((tp.getU().getUID() == e.getU().getUID() && tp.getV().getUID() == e.getV().getUID()) || (tp.getU().getUID() == e.getV().getUID() && tp.getV().getUID() == e.getU().getUID())) {
                dist = tp.getWeight();
                if (dist > 3) {
                    return 3;
                }
                return (int) dist;
            }
        }
        return (int) dist;

    }

    private double getDifference(Profile f1, Profile f2) {
        double def = 0;

        for (int i = 0; i < f1.profileLength(); i++) {
            if (!f1.getValue(i).equals(f2.getValue(i))) {
                def++;
            }
        }
        return def;
    }

}
