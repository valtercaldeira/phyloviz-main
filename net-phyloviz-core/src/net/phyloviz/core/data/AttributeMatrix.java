/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.phyloviz.core.data;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Valter
 */
public interface AttributeMatrix<T extends InfoNode> {
    
    /**
     * Add edges to final result
     * @param tp
     */
    public void addEdge(TaxaPair<Profile> tp);

    public ArrayList<? extends InfoEdge<T>> getEdges();
    
    public int size();

    public String info(InfoEdge<T> edge);
    
    public ArrayList<? extends InfoEdge<T>> getEdges(int idx);
    
    public Collection<T> getNodes(int idx);
    
    public int groupsSize();
    
    public ArrayList<Cluster> getClusters();
    
    public DistanceMatrix getDistance();

}
