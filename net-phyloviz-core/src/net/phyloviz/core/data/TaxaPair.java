/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.phyloviz.core.data;

/**
 *
 * @author Valter
 */
public class TaxaPair<T extends Profile> implements Comparable<TaxaPair<T>> {

    private T u;
    private T v;
    private double weight = 1;

    @Override
    public String toString() {
        return u.getID() + "--" + v.getID();
    }

    public void setWeight(double w) {
        weight = w;
    }

    public double getWeight() {
        return weight;
    }

    public TaxaPair(T u, T v) {
        if (u.getUID() > v.getUID()) {
            T x = u;
            u = v;
            v = x;
        }

        this.u = u;
        this.v = v;
    }

    public TaxaPair(T u, T v, double d) {
        this.weight = d;
        if (u.getUID() > v.getUID()) {
            T x = u;
            u = v;
            v = x;
        }

        this.u = u;
        this.v = v;
    }

    public T getU() {
        return u;
    }

    public T getV() {
        return v;
    }

    @Override
    public int compareTo(TaxaPair<T> e) {
        if (e.getU().getUID() == u.getUID() && e.getV().getUID() == v.getUID()) {
            return 0;
        }
        if (e.getU().getUID() == v.getUID() && e.getV().getUID() == u.getUID()) {
            return 0;
        }

        if (u.getUID() - e.getU().getUID() != 0) {
            return u.getUID() - e.getU().getUID();
        }

        return v.getUID() - e.getV().getUID();
    }

    public boolean equals(TaxaPair<T> e) {
        return compareTo(e) == 0;
    }

}
