/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.phyloviz.core.data;

import java.util.Collection;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Valter
 */
public abstract class AbstractDistanceMatrix<T extends Profile> implements DistanceMatrix, AbstractDistance<T> {

    public int Size;
    public TypingData td;


    public AbstractDistanceMatrix(TypingData<? extends Profile> otd) {
        Size = 0;
        td = otd;       
    }

    public AbstractDistanceMatrix(int s) {
        Size = s;
    }

}
