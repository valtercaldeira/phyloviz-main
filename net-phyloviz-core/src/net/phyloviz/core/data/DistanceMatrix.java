/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.phyloviz.core.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.TreeSet;

/**
 *
 * @author Valter
 */
public interface DistanceMatrix {

    /**
     * Returns a comparator, to compares TaxaPair, this comparator must be
 defined by method setComparators()
     */
    public Comparator<TaxaPair<Profile>> getTaxasPairComparator();


    /**
     * Used to set comparators, if you want set just one, put the value of the
     * other null
     *
     * @param Comparator of Taxas
     * @param Comparator of Profiles
     */
    public void setComparators(Comparator<TaxaPair<Profile>> compTaxas, Comparator<Profile> compProfile);

    public int getSize();
    
    public void incSize();

    public double getDistance(TaxaPair<Profile> e);

    public int getRatedDistance(TaxaPair<Profile> e);

    public int compareTaxasPairs(TaxaPair<Profile> e, TaxaPair<Profile> f);

    public int compareProfiles(Profile e, Profile f);

    public Collection<Profile> getAllProfiles();

    public ArrayList<TaxaPair<Profile>> getAllTaxasPairs();
    
    public TaxaPair<Profile> getFirstTaxaPairOrdered();
    
    public int getSizeOfAllTaxasPairs();

    public ArrayList<TaxaPair<Profile>> getTaxasPairs(int level);

    public int maxLevel();

    public double getAllDistFromProfile(int index);

    public void update(TaxaPair<Profile> edge, double dist);

    public void putDistance(TaxaPair<Profile> tp, Double def);

    public void removeProfile(Profile profile);
    
    public ArrayList<Double> getDistsFromProfile(int idx);
}
