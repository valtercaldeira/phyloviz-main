/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.phyloviz.core.data;

/**
 *
 * @author Valter
 */
public class InfoEdge<T extends InfoNode> implements Comparable<InfoEdge>{

    private boolean vis = true;
    private T u;
    private T v;
    private double weight = 1;
    
    @Override
    public String toString(){
        return u.toString() + "-" + v.toString();
    }

    public InfoEdge(T u, T v) {
        this.u = u;
        this.v = v;
    }

    public T getU() {
        return u;
    }

    public T getV() {
        return v;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setVisible(boolean vis) {
        this.vis = vis;
    }

    public boolean visible() {
        return vis;
    }

    @Override
    public int compareTo(InfoEdge o) {
        return (int) (weight - o.getWeight());
    }

}
